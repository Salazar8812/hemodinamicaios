//
//  Colors.swift
//  hemodinamica
//
//  Created by Charls Salazar on 27/04/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

extension UIColor {
    public convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    public convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
}

public class Colors {
    
    public static let color_blue : Int = 0x0E84E0
    public static let color_gray : Int = 0x7A7A7A
    public static let color_blue_light : Int = 0x95D1F0
    public static let color_second_blue : Int = 0x0CAFF8
    public static let color_background_app : Int = 0x061577
    public static let color_background_dialog : Int = 0x000054
    public static let color_yellow : Int = 0xFFEA13
    public static let color_white : Int = 0xFFFFFF
    public static let color_black : Int = 0x000000

    public static let color_green_background = 0x54A41A
    
    public static let color_green_strong = 0x366E10
    public static let color_green_bar = 0x366E10
    public static let color_red = 0xD43831
    
    public static let blue_dark_UIText = 0x111A21
    public static let blue_dark_background = 0x0C1116
    public static let blue_ligh_border = 0x1C222B
    public static let yellow_border = 0xCC8229
    public static let blue_text = 0x84878A
    public static let green_icon = 0x81AA54
}

