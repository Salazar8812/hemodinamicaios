//
//  SplasScreenViewController.swift
//  hemodinamica
//
//  Created by pc on 23/04/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class SplasScreenViewController: UIViewController {
    var counter = 0
    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delayLaunchIntroductionView()
    }
    
    func delayLaunchIntroductionView(){
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(actionDelayHome), userInfo: nil, repeats: true)
    }
    
    @objc func actionDelayHome() {
        counter += 1
        if(counter == 10){
            timer.invalidate()
            launchHome()
        }
    }
    
    func launchHome(){
        let storyboard : UIStoryboard = UIStoryboard(name: "MenuTab", bundle: nil)
        let vc : MenuTabViewController = storyboard.instantiateViewController(withIdentifier: "MenuTabViewController") as! MenuTabViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }

}
