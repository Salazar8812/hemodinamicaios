//
//  Tab1ViewController.swift
//  hemodinamica
//
//  Created by pc on 23/04/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class Tab1ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.tintColor = UIColor(netHex: Colors.color_white)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        navigationController?.navigationBar.backgroundColor = UIColor(netHex: Colors.blue_dark_background)
        navigationController?.navigationBar.barTintColor = UIColor(netHex: Colors.blue_dark_background)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController?.navigationBar.topItem?.title = "Retorno Venoso"
    }
}
