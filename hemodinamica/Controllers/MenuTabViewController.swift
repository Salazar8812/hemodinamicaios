//
//  MenuTabViewController.swift
//  hemodinamica
//
//  Created by pc on 23/04/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class MenuTabViewController: UIViewController {
    @IBOutlet weak var mContainerView: UIView!
    
    @IBOutlet weak var mTab1Button: UIButton!
    @IBOutlet weak var mTab2Button: UIButton!
    @IBOutlet weak var mTab3Button: UIButton!
    
    @IBOutlet weak var mTab1ImageView: UIImageView!
    @IBOutlet weak var mTab2ImageView: UIImageView!
    @IBOutlet weak var mTab3ImageView: UIImageView!
    
    @IBOutlet weak var mTab1Label: UILabel!
    @IBOutlet weak var mTab2Label: UILabel!
    @IBOutlet weak var mTab3Label: UILabel!
    
    override func viewDidLoad() {
         super.viewDidLoad()
        defaultColor()
        setAddTarget()
    }
    
    func defaultColor(){
        setColorImageSelected(mTapImage: mTab1ImageView, mTapLabel: mTab1Label, isSelected: true)
        setColorImageSelected(mTapImage: mTab2ImageView, mTapLabel: mTab2Label, isSelected: false)
        setColorImageSelected(mTapImage: mTab3ImageView, mTapLabel: mTab3Label, isSelected: false)
    }
    
    func setAddTarget(){
        setTitle(mTitle: "Retorno Venoso")
        mTab1Button.addTarget(self, action: #selector(selectTab1Action), for: .touchUpInside)
        mTab2Button.addTarget(self, action: #selector(selectTab2Action), for: .touchUpInside)
        mTab3Button.addTarget(self, action: #selector(selectTab3Action), for: .touchUpInside)
        loadController(mNameStoryboard: "Tab1ViewController",mTypeViewController: Tab1ViewController.self)
    }
    
    @objc func selectTab1Action(){
        setTitle(mTitle: "Retorno Venoso")
        setColorImageSelected(mTapImage: mTab1ImageView, mTapLabel: mTab1Label, isSelected: true)
        setColorImageSelected(mTapImage: mTab2ImageView, mTapLabel: mTab2Label, isSelected: false)
        setColorImageSelected(mTapImage: mTab3ImageView, mTapLabel: mTab3Label, isSelected: false)
        loadController(mNameStoryboard: "Tab1ViewController",mTypeViewController: Tab1ViewController.self)
    }
    
    @objc func selectTab2Action(){
        setTitle(mTitle: "Acoplamiento V/A")

        setColorImageSelected(mTapImage: mTab1ImageView, mTapLabel: mTab1Label, isSelected: false)
        setColorImageSelected(mTapImage: mTab2ImageView, mTapLabel: mTab2Label, isSelected: true)
        setColorImageSelected(mTapImage: mTab3ImageView, mTapLabel: mTab3Label, isSelected: false)
        
        loadController(mNameStoryboard: "Tab2ViewController",mTypeViewController: Tab2ViewController.self)
    }
    
    @objc func selectTab3Action(){
        setTitle(mTitle: "Taller Hemo/Respi")
        setColorImageSelected(mTapImage: mTab1ImageView, mTapLabel: mTab1Label, isSelected: false)
        setColorImageSelected(mTapImage: mTab2ImageView, mTapLabel: mTab2Label, isSelected: false)
        setColorImageSelected(mTapImage: mTab3ImageView, mTapLabel: mTab3Label, isSelected: true)
        
        loadController(mNameStoryboard: "Tab3ViewController",mTypeViewController: Tab3ViewController.self)
    }
    
    func loadController <T: UIViewController> (mNameStoryboard: String, mTypeViewController : T.Type){
        let news = self.storyboard?.instantiateViewController(withIdentifier: mNameStoryboard) as! T
        news.view.frame = mContainerView.bounds
        mContainerView.addSubview(news.view)
        addChild(news)
        news.didMove(toParent: self)
    }
    
    func setTitle(mTitle : String){
        self.navigationController?.navigationBar.tintColor = UIColor(netHex: Colors.color_white)
        navigationController?.navigationBar.backgroundColor = UIColor(netHex: Colors.blue_dark_background)
        navigationController?.navigationBar.barTintColor = UIColor(netHex: Colors.blue_dark_background)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController?.navigationBar.topItem?.title = mTitle
    }
    
    func setColorImageSelected(mTapImage : UIImageView, mTapLabel: UILabel, isSelected : Bool){
        if(isSelected){
            mTapLabel.textColor = UIColor.white
            mTapImage.image = mTapImage.image?.withRenderingMode(.alwaysTemplate)
            mTapImage.tintColor = UIColor.white
        }else{
            mTapLabel.textColor = UIColor(netHex: Colors.green_icon)
            mTapImage.image = mTapImage.image?.withRenderingMode(.alwaysTemplate)
            mTapImage.tintColor = UIColor(netHex: Colors.green_icon)
        }
    }

}
